package com.krzysztofbalana.myresume.data.workexperience

import com.krzysztofbalana.myresume.data.core.utils.NetworkStatus
import com.krzysztofbalana.myresume.data.workexperience.dtos.Job
import com.krzysztofbalana.myresume.data.workexperience.dtos.WorkExperienceDTO
import com.krzysztofbalana.myresume.data.workexperience.dtos.WorkexperienceItem
import com.krzysztofbalana.myresume.domain.common.exceptions.InternetConnectionException
import com.nhaarman.mockitokotlin2.mock
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.amshove.kluent.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import retrofit2.Call
import retrofit2.Response

class WorkExperienceRepositoryTest {

    lateinit var systemUnderTest: WorkExperienceRepository

    val mockCall = mock<Call<WorkExperienceDTO>> {
        on { execute() } itReturns responseBody()
    }
    val workExperienceWebApi: WorkExperienceWebApi = mock {
        on { workExperience() } itReturns mockCall
    }

    val workExperienceLocalStorageApi: WorkExperienceLocalStorageApi = mock {
        on { getWorkExperience() } itReturns mock()
    }

    val networkStatus: NetworkStatus = mock()

    @Before
    fun setUp() {
        systemUnderTest = WorkExperienceRepository(workExperienceWebApi, workExperienceLocalStorageApi, networkStatus)
    }

    @Test
    fun `should return list of workexperiences when connection ended successfully`() {
        simulateNoOfflineData()
        simulateNetworkIsActive()

        val result = systemUnderTest.getWorkExperience()
        assertThat(result).isNotEmpty
    }

    @Test(expected = InternetConnectionException::class)
    fun `should throw InternetConnectionException when there is no network available`() {
        simulateNoOfflineData()
        simulateNetworkIsInActive()

        systemUnderTest.getWorkExperience()
    }

    @Test(expected = UnknownError::class)
    fun `should throw UnknownError when something goes wrong`() {
        simulateNoOfflineData()
        simulateNetworkIsActive()
        When calling mockCall.execute() `it returns` emptyResponseBody()

        systemUnderTest.getWorkExperience()
    }

    @Test
    fun `if offline storage does not exists it should fetch from web and save to offline`() {
        simulateNetworkIsActive()

        systemUnderTest.getWorkExperience()

        Verify on workExperienceLocalStorageApi that workExperienceLocalStorageApi.getWorkExperience() was called
        VerifyNoInteractions on workExperienceWebApi
    }

    @Test
    fun `if offline storage exists it should fetch from online only`() {
        simulateNoOfflineData()
        simulateNetworkIsActive()

        val result = systemUnderTest.getWorkExperience()

        Verify on workExperienceLocalStorageApi that workExperienceLocalStorageApi.getWorkExperience() was called
        Verify on workExperienceWebApi that workExperienceWebApi.workExperience() was called
        Verify on workExperienceLocalStorageApi that workExperienceLocalStorageApi.saveWorkExperience(com.nhaarman.mockitokotlin2.any()) was called

        assertThat(result).isNotEmpty
    }

    private fun simulateNoOfflineData() {
        When calling workExperienceLocalStorageApi.getWorkExperience() itReturns null
    }

    private fun simulateNetworkIsActive() {
        When calling networkStatus.isActive() `it returns` true
    }

    private fun simulateNetworkIsInActive() {
        When calling networkStatus.isActive() `it returns` false
    }

    fun emptyResponseBody(): Response<WorkExperienceDTO> {
        return Response.error(404, ResponseBody.create(MediaType.parse(""), ""))
    }
    fun responseBody(): Response<WorkExperienceDTO> {
        return Response.success(WorkExperienceDTO(listOf(WorkexperienceItem(Job("test")))))
    }
}