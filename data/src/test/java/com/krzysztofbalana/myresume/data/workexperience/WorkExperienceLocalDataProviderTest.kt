package com.krzysztofbalana.myresume.data.workexperience

import android.content.Context
import android.content.SharedPreferences
import com.krzysztofbalana.myresume.data.workexperience.dtos.Job
import com.krzysztofbalana.myresume.data.workexperience.dtos.WorkExperienceDTO
import com.krzysztofbalana.myresume.data.workexperience.dtos.WorkexperienceItem
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.amshove.kluent.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class WorkExperienceLocalDataProviderTest {

    lateinit var systemUnderTest: WorkExperienceLocalDataProvider

    val internalEditor: SharedPreferences.Editor = mock {
        on { putString(any(), any()) } itReturns mock()
    }

    val editor: SharedPreferences.Editor = mock {
        on { putString(any(), any()) } itReturns internalEditor
        on { remove(any()) } itReturns internalEditor
    }

    val sharedPreferences: SharedPreferences = mock {
        on { edit() } itReturns editor
        on { getString(any(), any()) } itReturns "{\"workexperience\":[{\"job\":{\"yearFrom\":\"test\"}}]}"
    }
    val context: Context = mock {
        on { getSharedPreferences(any(), any()) } itReturns sharedPreferences
    }

    @Before
    fun setUp() {
        systemUnderTest = WorkExperienceLocalDataProvider(context)
    }

    @Test
    fun `should save dto`() {
        val dummyDto = WorkExperienceDTO(listOf(WorkexperienceItem(Job("test"))))

        systemUnderTest.saveWorkExperience(dummyDto)

        Verify on sharedPreferences that sharedPreferences.edit()
        Verify on internalEditor that internalEditor.commit()
    }

    @Test
    fun `should load dto`() {
        val result = systemUnderTest.getWorkExperience()

        assertThat(result!!.workexperience[0].job?.yearFrom).isEqualTo("test")
    }

    @Test
    fun `should return null if no dto stored`() {
        When calling sharedPreferences.getString(any(), any()) itReturns ""
        val result = systemUnderTest.getWorkExperience()

        assertThat(result).isNull()
    }
}