package com.krzysztofbalana.myresume.data.about

import com.krzysztofbalana.myresume.data.about.dtos.AboutDTO
import com.krzysztofbalana.myresume.data.core.utils.NetworkStatus
import com.krzysztofbalana.myresume.data.workexperience.WorkExperienceWebApi
import com.krzysztofbalana.myresume.data.workexperience.dtos.Job
import com.krzysztofbalana.myresume.data.workexperience.dtos.WorkExperienceDTO
import com.krzysztofbalana.myresume.data.workexperience.dtos.WorkexperienceItem
import com.krzysztofbalana.myresume.domain.common.exceptions.InternetConnectionException
import com.nhaarman.mockitokotlin2.mock
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.amshove.kluent.When
import org.amshove.kluent.`it returns`
import org.amshove.kluent.calling
import org.amshove.kluent.itReturns
import org.assertj.core.api.Assertions
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import retrofit2.Call
import retrofit2.Response

class AboutRepositoryTest {

    lateinit var systemUnderTest: AboutRepository

    val mockCall = mock<Call<AboutDTO>> {
        on { execute() } itReturns responseBody()
    }
    val aboutWebApi: AboutWebApi = mock {
        on { about() } itReturns mockCall
    }
    val networkStatus: NetworkStatus = mock()

    @Before
    fun setUp() {

        systemUnderTest = AboutRepository(aboutWebApi, networkStatus)
    }

    @Test
    fun `should return about dto when connection ended successfully`() {
        simulateNetworkIsActive()

        val result = systemUnderTest.getAbout()
        Assertions.assertThat(result.description).isNotBlank()
    }

    @Test(expected = InternetConnectionException::class)
    fun `should throw InternetConnectionException when there is no network available`() {
        simulateNetworkIsInActive()

        systemUnderTest.getAbout()
    }

    @Test(expected = UnknownError::class)
    fun `should throw UnknownError when something goes wrong`() {
        simulateNetworkIsActive()
        When calling mockCall.execute() `it returns` emptyResponseBody()

        systemUnderTest.getAbout()
    }

    fun emptyResponseBody(): Response<AboutDTO> {
        return Response.error(404, ResponseBody.create(MediaType.parse(""), ""))
    }
    fun responseBody(): Response<AboutDTO> {
        return Response.success(AboutDTO("descroptop"))
    }

    private fun simulateNetworkIsActive() {
        When calling networkStatus.isActive() `it returns` true
    }

    private fun simulateNetworkIsInActive() {
        When calling networkStatus.isActive() `it returns` false
    }

}