package com.krzysztofbalana.myresume.data.workexperience

import com.krzysztofbalana.myresume.data.core.utils.NetworkStatus
import com.krzysztofbalana.myresume.data.workexperience.dtos.WorkExperienceDTO
import com.krzysztofbalana.myresume.data.workexperience.dtos.WorkExperienceDTOMapper
import com.krzysztofbalana.myresume.domain.common.exceptions.InternetConnectionException
import com.krzysztofbalana.myresume.domain.usecases.workeexperience.WorkExperienceApi
import com.krzysztofbalana.myresume.domain.usecases.workeexperience.WorkExperienceEntity
import javax.inject.Inject
import javax.inject.Singleton

class WorkExperienceRepository @Inject constructor(
    private val workExperienceWebApi: WorkExperienceWebApi,
    private val workExperienceLocalStorageApi: WorkExperienceLocalStorageApi,
    private val networkStatus: NetworkStatus
) : WorkExperienceApi {

    override fun getWorkExperience(): List<WorkExperienceEntity> {
        val workExperience = workExperienceLocalStorageApi.getWorkExperience()
        return if (workExperience != null) {
            mapWorkExperienceDTO(workExperience)
        } else {
            fetchFromWeb()
        }
    }

    private fun fetchFromWeb(): List<WorkExperienceEntity> {
        return if (networkStatus.isActive()) {
            val response = workExperienceWebApi.workExperience().execute()
            if (response.body() != null) {
                workExperienceLocalStorageApi.saveWorkExperience(response.body()!!)
                mapWorkExperienceDTO(response.body()!!)
            } else {
                throw UnknownError()
            }
        } else {
            throw InternetConnectionException()
        }
    }

    private fun mapWorkExperienceDTO(dto: WorkExperienceDTO): List<WorkExperienceEntity> =
        WorkExperienceDTOMapper.map(dto)
}