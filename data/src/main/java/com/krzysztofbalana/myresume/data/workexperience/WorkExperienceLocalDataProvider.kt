package com.krzysztofbalana.myresume.data.workexperience

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.google.gson.Gson
import com.krzysztofbalana.myresume.data.workexperience.dtos.WorkExperienceDTO
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WorkExperienceLocalDataProvider @Inject constructor(context: Context): WorkExperienceLocalStorageApi {

    private val OFFLINE_STORAGE_KEY = "work_experience_offline_storage_key_value"

    private val offlineStorage: SharedPreferences = context.getSharedPreferences("work_experience_offline_storage", MODE_PRIVATE)

    private val gson = Gson()

    override fun saveWorkExperience(dto: WorkExperienceDTO) {
        offlineStorage.edit().putString(OFFLINE_STORAGE_KEY, gson.toJson(dto)).commit()
    }

    override fun getWorkExperience(): WorkExperienceDTO? {
        val dto = gson.fromJson(offlineStorage.getString(OFFLINE_STORAGE_KEY, ""), WorkExperienceDTO::class.java
        )

        /**
         * It's a very sophisticated way of removing cached data. Don't even try to understand it. Pure magic.
         */
        if (dto != null) {
            offlineStorage.edit().remove(OFFLINE_STORAGE_KEY).commit()
        }
        return dto
    }
}
