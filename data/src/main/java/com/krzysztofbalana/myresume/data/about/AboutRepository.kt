package com.krzysztofbalana.myresume.data.about

import com.krzysztofbalana.myresume.data.about.dtos.AboutMapperDTOMapper
import com.krzysztofbalana.myresume.data.core.utils.NetworkStatus
import com.krzysztofbalana.myresume.domain.about.AboutApi
import com.krzysztofbalana.myresume.domain.about.AboutEntity
import com.krzysztofbalana.myresume.domain.common.exceptions.InternetConnectionException
import javax.inject.Inject

class AboutRepository @Inject constructor(private val aboutWebApi: AboutWebApi, private val networkStatus: NetworkStatus): AboutApi {

    override fun getAbout(): AboutEntity {
        return if (networkStatus.isActive()) {
            val response = aboutWebApi.about().execute()
            if (response.body() != null) {
                AboutMapperDTOMapper.map(response.body()!!)
            } else {
                throw UnknownError()
            }
        } else {
            throw InternetConnectionException()
        }
    }
}