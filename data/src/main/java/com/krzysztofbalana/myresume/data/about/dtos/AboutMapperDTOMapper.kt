package com.krzysztofbalana.myresume.data.about.dtos

import com.krzysztofbalana.myresume.domain.about.AboutEntity

object AboutMapperDTOMapper {

    fun map(dto: AboutDTO) : AboutEntity {
        return AboutEntity(dto.description)
    }
}