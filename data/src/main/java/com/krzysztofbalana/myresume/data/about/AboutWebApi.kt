package com.krzysztofbalana.myresume.data.about

import com.krzysztofbalana.myresume.data.about.dtos.AboutDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface AboutWebApi {

    @Headers("Content-Type: application/json")
    @GET("/kbalana/62f77efffb7309029482216259ad1c67/raw/86825a062f1cbab2c0b41153d23d297924be6085/about")
    fun about(): Call<AboutDTO>
}