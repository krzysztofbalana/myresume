package com.krzysztofbalana.myresume.data.workexperience.dtos

import com.google.gson.annotations.SerializedName

data class WorkexperienceItem(

	@field:SerializedName("job")
	val job: Job? = null
)