package com.krzysztofbalana.myresume.data.workexperience.dtos

import com.krzysztofbalana.myresume.domain.usecases.workeexperience.WorkExperienceEntity

object WorkExperienceDTOMapper {

    fun map(dto: WorkExperienceDTO): List<WorkExperienceEntity> = dto.workexperience.map {
        WorkExperienceEntity(
            it.job?.title ?: notAvailable(),
            it.job?.company ?: notAvailable(),
            it.job?.yearFrom ?: notAvailable(),
            it.job?.monthFrom ?: notAvailable(),
            it.job?.yearTo ?: notAvailable(),
            it.job?.monthTo ?: notAvailable(),
            it.job?.description ?: notAvailable(),
            it.job?.position ?: 0
        )
    }

    private fun notAvailable() = "N/A"
}