package com.krzysztofbalana.myresume.data.about.dtos

import com.google.gson.annotations.SerializedName

data class AboutDTO(

	@field:SerializedName("description")
	val description: String
)