package com.krzysztofbalana.myresume.data.workexperience

import com.krzysztofbalana.myresume.data.workexperience.dtos.WorkExperienceDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface WorkExperienceWebApi {

    @Headers("Content-Type: application/json")
    @GET("/kbalana/1863ebcf761d5c55e0848762087c98f0/raw/d68412d48ab2546ca4c3566dcc6e78c450f12dbe/work_experience")
    fun workExperience(): Call<WorkExperienceDTO>
}