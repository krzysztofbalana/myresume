package com.krzysztofbalana.myresume.data.workexperience.dtos

import com.google.gson.annotations.SerializedName

data class Job(

	@field:SerializedName("yearFrom")
	val yearFrom: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("company")
	val company: String? = null,

	@field:SerializedName("monthFrom")
	val monthFrom: String? = null,

	@field:SerializedName("position")
	val position: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("monthTo")
	val monthTo: String? = null,

	@field:SerializedName("yearTo")
	val yearTo: String? = null
)