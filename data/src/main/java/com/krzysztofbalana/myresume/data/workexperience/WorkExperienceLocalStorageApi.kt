package com.krzysztofbalana.myresume.data.workexperience

import com.krzysztofbalana.myresume.data.workexperience.dtos.WorkExperienceDTO

interface WorkExperienceLocalStorageApi {

    fun getWorkExperience(): WorkExperienceDTO?
    fun saveWorkExperience(dto: WorkExperienceDTO)
}
