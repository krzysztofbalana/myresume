package com.krzysztofbalana.myresume.data.workexperience.dtos

import com.google.gson.annotations.SerializedName

data class WorkExperienceDTO(

	@field:SerializedName("workexperience")
	val workexperience: List<WorkexperienceItem>
)