package com.krzysztofbalana.myresume.common.di

import com.krzysztofbalana.myresume.domain.usecases.workeexperience.GetWorkExperienceUseCase
import com.krzysztofbalana.myresume.domain.usecases.workeexperience.WorkExperienceApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DomainModule {

    @Provides
    @Singleton
    fun provideWorkExperienceUseCase(workExperienceApi: WorkExperienceApi) = GetWorkExperienceUseCase(workExperienceApi)
}
