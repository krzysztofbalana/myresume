package com.krzysztofbalana.myresume.common

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.net.Uri

class ExternalNavigator(private val context: Context) {

    fun dialNumber(phoneNumber: String) {
        context.startActivity(
            Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null)).apply { flags = FLAG_ACTIVITY_NEW_TASK }
        )
    }

    fun browse(urlString: String) {
        val browserIntent =
            Intent(Intent.ACTION_VIEW, Uri.parse(urlString)).apply { flags = FLAG_ACTIVITY_NEW_TASK }
        context.startActivity(browserIntent)
    }

    fun mailTo(emailAddress: String) {
        val emailIntent = Intent(Intent.ACTION_SENDTO).apply { flags = FLAG_ACTIVITY_NEW_TASK }
        emailIntent.data = Uri.parse("mailto:$emailAddress")
        context.startActivity(emailIntent)
    }
}