package com.krzysztofbalana.myresume.common

import com.krzysztofbalana.myresume.ui.about.AboutActivity
import com.krzysztofbalana.myresume.ui.about.AboutModule
import com.krzysztofbalana.myresume.ui.menuscreen.MenuScreenActivity
import com.krzysztofbalana.myresume.ui.menuscreen.MenuScreenModule
import com.krzysztofbalana.myresume.ui.workexperience.WorkExperienceActivity
import com.krzysztofbalana.myresume.ui.workexperience.WorkExperienceModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [MenuScreenModule::class])
    abstract fun bindMenuScreenActivity(): MenuScreenActivity

    @ContributesAndroidInjector(modules = [WorkExperienceModule::class])
    abstract fun bindWorkExperienceActivity(): WorkExperienceActivity

    @ContributesAndroidInjector(modules = [AboutModule::class])
    abstract fun bindWorkAboutActivity(): AboutActivity
}