package com.krzysztofbalana.myresume.common.di

import android.app.Application
import com.krzysztofbalana.myresume.common.ActivityBuilder
import com.krzysztofbalana.myresume.common.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuilder::class,
        NetworkingModule::class,
        AppModule::class,
        DomainModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {

    override fun inject(app: App)
    fun inject(instance: DaggerApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}