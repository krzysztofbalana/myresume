package com.krzysztofbalana.myresume.common.di

import android.app.Application
import android.content.Context
import com.krzysztofbalana.myresume.common.ExternalNavigator
import com.krzysztofbalana.myresume.data.about.AboutRepository
import com.krzysztofbalana.myresume.data.about.AboutWebApi
import com.krzysztofbalana.myresume.data.core.utils.NetworkStatus
import com.krzysztofbalana.myresume.data.workexperience.WorkExperienceLocalDataProvider
import com.krzysztofbalana.myresume.data.workexperience.WorkExperienceLocalStorageApi
import com.krzysztofbalana.myresume.data.workexperience.WorkExperienceWebApi
import com.krzysztofbalana.myresume.data.workexperience.WorkExperienceRepository
import com.krzysztofbalana.myresume.domain.about.AboutApi
import com.krzysztofbalana.myresume.domain.usecases.workeexperience.WorkExperienceApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    fun provideResources(context: Context) = context.resources

    @Provides
    @Singleton
    fun provideExternalNavigator(context: Context) = ExternalNavigator(context)

    @Provides
    @Singleton
    fun providesWorkExperienceLocalStorageApi(context: Context): WorkExperienceLocalStorageApi =
        WorkExperienceLocalDataProvider(context)

    @Provides
    @Singleton
    fun provideWorkExperienceApi(
        workExperienceFeedApi: WorkExperienceWebApi,
        workExperienceLocalStorageApi: WorkExperienceLocalStorageApi,
        networkStatus: NetworkStatus
    ): WorkExperienceApi =
        WorkExperienceRepository(
            workExperienceFeedApi,
            workExperienceLocalStorageApi,
            networkStatus
        )

    @Provides
    @Singleton
    fun provideAboutRepository(aboutWebApi: AboutWebApi, networkStatus: NetworkStatus): AboutApi =
        AboutRepository(aboutWebApi, networkStatus)
}