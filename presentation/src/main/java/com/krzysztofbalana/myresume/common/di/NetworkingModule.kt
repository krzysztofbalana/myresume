package com.krzysztofbalana.myresume.common.di

import android.content.Context
import com.krzysztofbalana.myresume.data.about.AboutWebApi
import com.krzysztofbalana.myresume.data.core.utils.NetworkStatus
import com.krzysztofbalana.myresume.data.workexperience.WorkExperienceWebApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkingModule {

    @Provides
    @Singleton
    fun provideOkHttpBuilder() = OkHttpClient.Builder()

    @Provides
    @Singleton
    fun provideNetworkStatus(context: Context) =
        NetworkStatus(context)

    @Provides
    @Singleton
    fun provideRetrofitBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
    }

    @Provides
    @Singleton
    fun provideWorkExperienceWebApi(
        retrofitBuilder: Retrofit.Builder,
        okHttpClientBuilder: OkHttpClient.Builder
    ): WorkExperienceWebApi =
        retrofitBuilder
            .baseUrl("https://gist.githubusercontent.com")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClientBuilder.build())
            .build()
            .create(WorkExperienceWebApi::class.java)

    @Provides
    @Singleton
    fun provideAboutWebApi(
        retrofitBuilder: Retrofit.Builder,
        okHttpClientBuilder: OkHttpClient.Builder
    ): AboutWebApi =
        retrofitBuilder
            .baseUrl("https://gist.githubusercontent.com")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClientBuilder.build())
            .build()
            .create(AboutWebApi::class.java)
}
