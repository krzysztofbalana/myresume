package com.krzysztofbalana.myresume.common.extensions

import android.app.Activity
import android.content.Intent

fun Activity.navigateTo(intent: Intent) {
    this.startActivity(intent)
}