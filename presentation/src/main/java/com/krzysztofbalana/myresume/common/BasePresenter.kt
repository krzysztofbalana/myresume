package com.krzysztofbalana.myresume.common

interface BasePresenter<V>{

    fun run()

    fun setView(view: V)
}