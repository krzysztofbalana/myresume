package com.krzysztofbalana.myresume.ui.menuscreen

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.krzysztofbalana.myresume.R

class MenuScreenMenuItemsAdapter(private val menuItems: List<String>, private val onItemClickListener: (Int) -> Unit): RecyclerView.Adapter<MenuScreenMenuItemsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int
    ): ViewHolder {
        val layoutInflater =
            parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return ViewHolder(
            layoutInflater.inflate(R.layout.simple_menu_item, parent, false)
        )
    }

    override fun getItemCount() = menuItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.findViewById<TextView>(R.id.simple_menu_item_text_view).text = menuItems[position]
        holder.itemView.findViewById<TextView>(R.id.simple_menu_item_text_view).setOnClickListener { onItemClickListener.invoke(position) }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}