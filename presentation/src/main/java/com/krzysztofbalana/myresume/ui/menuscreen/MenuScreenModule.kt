package com.krzysztofbalana.myresume.ui.menuscreen

import dagger.Binds
import dagger.Module

@Module
abstract class MenuScreenModule {

    @Binds
    abstract fun providePresenter(presenter: MenuScreenPresenter): MenuScreen.Presenter

    @Binds
    abstract fun provideDataFactory(dataFactory: MenuScreenDataFactory): MenuScreen.ViewDataFactory
}