package com.krzysztofbalana.myresume.ui.about

import android.os.Bundle
import com.krzysztofbalana.myresume.R
import com.krzysztofbalana.myresume.common.BaseActivity
import kotlinx.android.synthetic.main.activity_about.*
import kotlinx.android.synthetic.main.activity_work_experience.work_experience_toolbar
import javax.inject.Inject

class AboutActivity : BaseActivity(), AboutScreen.View {
    private lateinit var toolbar: androidx.appcompat.widget.Toolbar

    @Inject
    lateinit var presenter: AboutScreen.Presenter

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun setData(data: AboutScreen.ViewData) {
        about_simple_description.text = data.description
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        this.toolbar = work_experience_toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.title = this.resources.getString(R.string.about)
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }

        presenter.setView(this)
        presenter.run()
    }
}
