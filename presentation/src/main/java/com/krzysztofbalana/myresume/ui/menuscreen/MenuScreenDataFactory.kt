package com.krzysztofbalana.myresume.ui.menuscreen

import android.content.res.Resources
import com.krzysztofbalana.myresume.R
import javax.inject.Inject

class MenuScreenDataFactory @Inject constructor(private val resources: Resources) : MenuScreen.ViewDataFactory {

    override fun prepareData(): MenuScreen.ViewData {
        return MenuScreen.ViewData(
            R.drawable.my_face,
            resources.getStringArray(R.array.menu_items).toList(),
            listOf(
                MenuScreen.SocialButton.PHONE,
                MenuScreen.SocialButton.MAIL,
                MenuScreen.SocialButton.LINKEDIN
            )
        )
    }
}