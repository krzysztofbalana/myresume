package com.krzysztofbalana.myresume.ui.workexperience

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.krzysztofbalana.myresume.R

class WorkExperienceListRowViewHolder(private val view: View):  RecyclerView.ViewHolder(view) {

    fun bind(job: WorkExperience.Job) {
        view.findViewById<TextView>(R.id.job_title).text = job.title
        view.findViewById<TextView>(R.id.work_experience_time).text = job.time
        view.findViewById<TextView>(R.id.work_experience_description).text = job.descritpion
    }
}