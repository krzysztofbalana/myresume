package com.krzysztofbalana.myresume.ui.about

import com.krzysztofbalana.myresume.domain.about.AboutEntity
import com.krzysztofbalana.myresume.domain.about.GetAboutUseCase
import com.krzysztofbalana.myresume.domain.common.Result
import javax.inject.Inject

class AboutPresenter @Inject constructor(
    private val getAboutUseCase: GetAboutUseCase
) : AboutScreen.Presenter {

    private lateinit var view: AboutScreen.View

    override fun getAbout(callback: (Result<AboutEntity>) -> Unit) {
        getAboutUseCase.execute(Unit) { callback.invoke(it) }
    }

    override fun run() {
        getAbout { result ->
            when (result) {
                is Result.Success -> {
                    view.setData(AboutScreen.ViewData(result.value.description))
                }
                is Result.Failure -> { //handle failure on ui. skipped intentionally
                }
            }
        }
    }

    override fun setView(view: AboutScreen.View) {
        this.view = view
    }
}