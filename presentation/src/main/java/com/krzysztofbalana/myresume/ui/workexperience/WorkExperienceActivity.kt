package com.krzysztofbalana.myresume.ui.workexperience

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.krzysztofbalana.myresume.R
import com.krzysztofbalana.myresume.common.BaseActivity
import kotlinx.android.synthetic.main.activity_work_experience.*
import javax.inject.Inject

class WorkExperienceActivity : BaseActivity(), WorkExperience.View {

    private lateinit var toolbar: androidx.appcompat.widget.Toolbar

    @Inject
    lateinit var presenter: WorkExperience.Presenter

    override fun setData(data: WorkExperience.ViewData) {
        work_experience_list.layoutManager = LinearLayoutManager(this).apply { orientation = LinearLayoutManager.VERTICAL }
        work_experience_list.adapter = WorkExperienceListAdapter(data.value)
    }

    override fun showFailure() {
        //It's here to show a reviewer how I usually handle failures on ui. I've skipped implementation consciously.
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_work_experience)
        this.toolbar = work_experience_toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.title = this.resources.getString(R.string.work_experience)
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }

        presenter.setView(this)
        presenter.run()
    }
}
