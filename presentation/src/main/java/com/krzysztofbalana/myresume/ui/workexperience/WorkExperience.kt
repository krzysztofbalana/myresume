package com.krzysztofbalana.myresume.ui.workexperience

import com.krzysztofbalana.myresume.common.BasePresenter
import com.krzysztofbalana.myresume.domain.common.Result
import com.krzysztofbalana.myresume.domain.usecases.workeexperience.WorkExperienceEntity

interface WorkExperience {

    interface Presenter : BasePresenter<View> {
        fun getWorkExperience(callback: (Result<List<WorkExperienceEntity>>) -> Unit)
    }

    interface View {
        fun setData(data: ViewData)
        fun showFailure()
    }

    interface ViewDataFactory {
        fun prepareData(domainData: List<WorkExperienceEntity>): ViewData
    }

    class ViewData(val value: List<Job>)

    data class Job(val title: String, val company: String, val time: String, val descritpion: String)
}