package com.krzysztofbalana.myresume.ui.about

import dagger.Binds
import dagger.Module

@Module
abstract class AboutModule {

    @Binds
    abstract fun providePresenter(presenter: AboutPresenter): AboutScreen.Presenter
}