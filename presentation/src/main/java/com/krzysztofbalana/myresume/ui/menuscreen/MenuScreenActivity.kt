package com.krzysztofbalana.myresume.ui.menuscreen

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.krzysztofbalana.myresume.common.BaseActivity
import com.krzysztofbalana.myresume.common.extensions.navigateTo
import com.krzysztofbalana.myresume.ui.about.AboutActivity
import com.krzysztofbalana.myresume.ui.menuscreen.MenuScreen.SocialButton
import com.krzysztofbalana.myresume.ui.menuscreen.MenuScreen.UiEvent.OnSocialButtonClicked
import com.krzysztofbalana.myresume.ui.workexperience.WorkExperience
import com.krzysztofbalana.myresume.ui.workexperience.WorkExperienceActivity
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MenuScreenActivity: BaseActivity(),
    MenuScreen.View {

    private lateinit var uiEventsListener: (MenuScreen.UiEvent) -> Unit

    @Inject
    lateinit var presenter: MenuScreen.Presenter

    override fun setUiEventsListener(listener: (MenuScreen.UiEvent) -> Unit) {
        this.uiEventsListener = listener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.krzysztofbalana.myresume.R.layout.activity_main)
        presenter.setView(this)
        presenter.run()
    }

    override fun setData(data: MenuScreen.ViewData) {
        initAvatar(data.avatarResource)
        initMenu(data.menuItems)
        initSocialButtons(data.socialItems)
    }

    private fun initAvatar(avatarResource: Int) {
        Glide.with(this).load(this.getDrawable(avatarResource)).apply(RequestOptions.circleCropTransform()).into(menu_screen_avatar)
    }

    private fun initMenu(menuItems: List<String>) {
        menu_screen_menu_items.layoutManager =
            LinearLayoutManager(this).apply { orientation = LinearLayoutManager.VERTICAL }
        menu_screen_menu_items.adapter =
            MenuScreenMenuItemsAdapter(menuItems) {
                handleItemClick(menuItems, it)
            }
    }

    private fun handleItemClick(menuItems: List<String>, selectedItem: Int) {
        when (selectedItem) {
            0 -> navigateTo(Intent(this, AboutActivity::class.java))
            1 -> navigateTo(Intent(this, WorkExperienceActivity::class.java))
        }
    }

    private fun initSocialButtons(socialItems: List<SocialButton>) {
        socialItems.forEach { socialButton ->
            when (socialButton) {
                SocialButton.PHONE -> menu_screen_social_phone.setOnClickListener {
                    uiEventsListener.invoke(OnSocialButtonClicked(SocialButton.PHONE))
                }
                SocialButton.MAIL -> menu_screen_social_mail.setOnClickListener {
                    uiEventsListener.invoke(OnSocialButtonClicked(SocialButton.MAIL))
                }
                SocialButton.LINKEDIN -> menu_screen_social_linkedin.setOnClickListener {
                    uiEventsListener.invoke(OnSocialButtonClicked(SocialButton.LINKEDIN))
                }
            }
        }
    }
}
