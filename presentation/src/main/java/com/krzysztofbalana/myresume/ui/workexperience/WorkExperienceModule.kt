package com.krzysztofbalana.myresume.ui.workexperience

import dagger.Binds
import dagger.Module

@Module
abstract class WorkExperienceModule {

    @Binds
    abstract fun provideWorkExperiencePresenter(presenter: WorkExperiencePresenter): WorkExperience.Presenter

    @Binds
    abstract fun provideDataFactory(dataFactory: WorkExperienceDataFactory): WorkExperience.ViewDataFactory
}