package com.krzysztofbalana.myresume.ui.workexperience

import com.krzysztofbalana.myresume.domain.usecases.workeexperience.WorkExperienceEntity
import javax.inject.Inject

class WorkExperienceDataFactory @Inject constructor() : WorkExperience.ViewDataFactory {

    override fun prepareData(domainData: List<WorkExperienceEntity>): WorkExperience.ViewData {
        return WorkExperience.ViewData(domainData.map {
            WorkExperience.Job(
                it.title.toUpperCase(),
                it.company,
                mergeTime(it.yearFrom, it.monthFrom, it.yearTo, it.monthTo),
                it.descritpion
            )
        })
    }

    private fun mergeTime(
        yearFrom: String,
        monthFrom: String,
        yearTo: String,
        monthTo: String
    ): String {
        val workedOrStillWorking = prepareTextWhenStillWorkingOrWorked(yearTo, monthTo)
        return "${monthFrom.toUpperCase()} ${yearFrom.toUpperCase()} - $workedOrStillWorking"
    }

    private fun prepareTextWhenStillWorkingOrWorked(yearTo: String, monthTo: String): String =
        if (yearTo.isBlank() || monthTo.isBlank()) {
            "NOW"
        } else {
            "${monthTo.toUpperCase()} ${yearTo.toUpperCase()}"
        }
}