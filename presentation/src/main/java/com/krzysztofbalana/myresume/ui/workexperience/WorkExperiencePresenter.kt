package com.krzysztofbalana.myresume.ui.workexperience

import com.krzysztofbalana.myresume.domain.common.Result
import com.krzysztofbalana.myresume.domain.usecases.workeexperience.GetWorkExperienceUseCase
import com.krzysztofbalana.myresume.domain.usecases.workeexperience.WorkExperienceEntity
import javax.inject.Inject

class WorkExperiencePresenter @Inject constructor(
    private val workExperienceUseCase: GetWorkExperienceUseCase,
    private val viewDataFactory: WorkExperience.ViewDataFactory
) : WorkExperience.Presenter {
    private lateinit var view: WorkExperience.View

    override fun getWorkExperience(callback: (Result<List<WorkExperienceEntity>>) -> Unit) {
        workExperienceUseCase.execute(Unit) { callback.invoke(it) }
    }

    override fun run() {
        getWorkExperience { result ->
            when (result) {
                is Result.Success -> {
                    handleSuccess(result.value)
                }
                is Result.Failure -> {
                    handleFailure()
                }
            }
        }
    }

    private fun handleSuccess(value: List<WorkExperienceEntity>) {
        viewDataFactory.prepareData(value).let {
            view.setData(it)
        }
    }

    private fun handleFailure() {
        view.showFailure()
    }

    override fun setView(view: WorkExperience.View) {
        this.view = view
    }
}