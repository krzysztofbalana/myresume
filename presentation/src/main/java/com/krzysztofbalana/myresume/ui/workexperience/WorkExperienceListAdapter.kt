package com.krzysztofbalana.myresume.ui.workexperience

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.krzysztofbalana.myresume.R

class WorkExperienceListAdapter(private val jobsDescription: List<WorkExperience.Job>) : RecyclerView.Adapter<WorkExperienceListRowViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkExperienceListRowViewHolder {
        val layoutInflater = parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = layoutInflater.inflate(R.layout.view_work_experience_row, parent, false)
        return WorkExperienceListRowViewHolder(view)
    }

    override fun getItemCount(): Int = jobsDescription.size

    override fun onBindViewHolder(holder: WorkExperienceListRowViewHolder, position: Int) {
        holder.bind(jobsDescription[position])
    }
}