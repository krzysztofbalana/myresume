package com.krzysztofbalana.myresume.ui.menuscreen

import com.krzysztofbalana.myresume.common.BasePresenter

interface MenuScreen {

    interface Presenter : BasePresenter<View>

    interface View {
        fun setUiEventsListener(listener: (UiEvent) -> Unit)
        fun setData(data: ViewData)
    }

    interface ViewDataFactory {
        fun prepareData(): ViewData
    }

    class ViewData(val avatarResource: Int, val menuItems: List<String>, val socialItems: List<SocialButton>)

    sealed class UiEvent {
        class OnSocialButtonClicked(val type: SocialButton) : UiEvent()
    }

    enum class SocialButton(val itemValue: String) {
        LINKEDIN("https://www.linkedin.com/in/krzysztofbalana/"), MAIL("krzysztof.balana@gmail.com"), PHONE("+48 793 912 916")
    }
}