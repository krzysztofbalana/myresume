package com.krzysztofbalana.myresume.ui.about

import com.krzysztofbalana.myresume.common.BasePresenter
import com.krzysztofbalana.myresume.domain.about.AboutEntity
import com.krzysztofbalana.myresume.domain.common.Result

interface AboutScreen {

    interface Presenter : BasePresenter<View> {
        fun getAbout(callback: (Result<AboutEntity>) -> Unit)
    }

    interface View {
        fun setData(data: ViewData)
    }

    class ViewData(val description: String)
}