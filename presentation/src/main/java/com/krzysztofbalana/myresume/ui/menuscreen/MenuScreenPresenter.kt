package com.krzysztofbalana.myresume.ui.menuscreen

import com.krzysztofbalana.myresume.common.ExternalNavigator
import com.krzysztofbalana.myresume.ui.menuscreen.MenuScreen.SocialButton.*
import com.krzysztofbalana.myresume.ui.menuscreen.MenuScreen.UiEvent.OnSocialButtonClicked
import javax.inject.Inject

class MenuScreenPresenter @Inject constructor(
    private val menuScreenDataFactory: MenuScreen.ViewDataFactory,
    private val externalNavigator: ExternalNavigator
) : MenuScreen.Presenter {
    private lateinit var view: MenuScreen.View

    override fun run() {
        val viewData = menuScreenDataFactory.prepareData()
        view.setUiEventsListener { handleEvent(it) }
        view.setData(viewData)
    }

    private fun handleEvent(uiEvent: MenuScreen.UiEvent) {
        when (uiEvent) {
            is OnSocialButtonClicked -> handleSocialButtonEvent(uiEvent)
        }
    }

    private fun handleSocialButtonEvent(event: OnSocialButtonClicked) {
        when (event.type) {
            MAIL -> externalNavigator.mailTo(MAIL.itemValue)
            PHONE -> externalNavigator.dialNumber(PHONE.itemValue)
            LINKEDIN -> externalNavigator.browse(LINKEDIN.itemValue)
        }
    }

    override fun setView(view: MenuScreen.View) {
        this.view = view
    }
}