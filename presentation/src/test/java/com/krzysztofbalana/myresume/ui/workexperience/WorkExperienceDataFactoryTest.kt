package com.krzysztofbalana.myresume.ui.workexperience

import com.krzysztofbalana.myresume.domain.usecases.workeexperience.WorkExperienceEntity
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before

import org.junit.Test

/**
 * I've intentionally omitted obvious unit tests. Just saving time :P
 */
class WorkExperienceDataFactoryTest {

    lateinit var systemUnderTest: WorkExperienceDataFactory

    @Before
    fun setUp() {
        systemUnderTest = WorkExperienceDataFactory()
    }

    @Test
    fun `should format work time accordingly`() {
        val expectedYearFrom = "2019"
        val expectedYearTo = "2020"
        val expectedMonthFrom = "January"
        val expectedMonthTo = "December"
        val expectedTimeFormat = "${expectedMonthFrom.toUpperCase()} ${expectedYearFrom.toUpperCase()} - ${expectedMonthTo.toUpperCase()} ${expectedYearTo.toUpperCase()}"
        val result = systemUnderTest.prepareData(prepareDummyData(expectedYearFrom, expectedMonthFrom, expectedMonthTo, expectedYearTo))

        assertThat(result.value[0].time).isEqualTo(expectedTimeFormat)
    }

    private fun prepareDummyData(
        expectedYearFrom: String,
        expectedMonthFrom: String,
        expectedMonthTo: String,
        expectedYearTo: String
    ): List<WorkExperienceEntity> {
        return listOf(
            WorkExperienceEntity(
                "title",
                "company",
                expectedYearFrom,
                expectedMonthFrom,
                expectedYearTo,
                expectedMonthTo,
                "Description",
                1
            )
        )
    }
}