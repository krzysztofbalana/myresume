package com.krzysztofbalana.myresume.ui

import com.krzysztofbalana.myresume.common.ExternalNavigator
import com.krzysztofbalana.myresume.ui.menuscreen.MenuScreen
import com.krzysztofbalana.myresume.ui.menuscreen.MenuScreen.SocialButton.MAIL
import com.krzysztofbalana.myresume.ui.menuscreen.MenuScreen.UiEvent.OnSocialButtonClicked
import com.krzysztofbalana.myresume.ui.menuscreen.MenuScreenPresenter
import com.nhaarman.mockitokotlin2.*
import org.amshove.kluent.Verify
import org.amshove.kluent.on
import org.amshove.kluent.that
import org.junit.Test

class MenuScreenPresenterTest {

    lateinit var systemUnderTest: MenuScreenPresenter
    val view: MenuScreen.View = mock()
    val viewDataFactory: MenuScreen.ViewDataFactory = mock {
        on { prepareData() } doReturn mock()
    }
    val externalNavigator: ExternalNavigator = mock()

    @org.junit.Before
    fun setUp() {
        systemUnderTest = MenuScreenPresenter(viewDataFactory, externalNavigator)
        systemUnderTest.setView(view)
    }

    @Test
    fun `should set uiEvent listener upon run method launch`() {
        systemUnderTest.run()

        Verify on view that view.setUiEventsListener(any())
    }

    @Test
    fun `should fetch data to display on view`() {
        systemUnderTest.run()

        Verify on view that view.setData(any())
    }

    @Test
    fun `should handle ui events action and pass them to an external navigator accordingly`() {
        systemUnderTest.run()

        argumentCaptor<(MenuScreen.UiEvent) -> Unit>().apply {
            Verify on view that view.setUiEventsListener(capture())

            firstValue.invoke(OnSocialButtonClicked(MAIL))

            Verify on externalNavigator that externalNavigator.mailTo(eq(MAIL.itemValue))
        }
    }
}