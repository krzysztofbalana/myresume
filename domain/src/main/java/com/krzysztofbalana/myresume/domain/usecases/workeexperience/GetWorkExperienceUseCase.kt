package com.krzysztofbalana.myresume.domain.usecases.workeexperience

import com.krzysztofbalana.myresume.domain.common.Result
import com.krzysztofbalana.myresume.domain.common.UseCase
import com.krzysztofbalana.myresume.domain.common.exceptions.InternetConnectionException
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetWorkExperienceUseCase @Inject constructor(
    private val workExperienceProvider: WorkExperienceApi
) : UseCase<Result<List<WorkExperienceEntity>>, Unit>(Dispatchers.Main, Dispatchers.IO) {
    override suspend fun buildUseCase(param: Unit): Result<List<WorkExperienceEntity>> {

        return try {
            val data = workExperienceProvider.getWorkExperience()
            if (data.isNotEmpty()) {
                Result.Success(data.sortedBy { it.position })
            } else {
                Result.Failure()
            }
        } catch (e: InternetConnectionException) {
            Result.Failure(e) //normally i should handle error with specific message for ui, so it could be presented
        } catch (e: Exception) {
            Result.Failure(e)
        }
    }
}