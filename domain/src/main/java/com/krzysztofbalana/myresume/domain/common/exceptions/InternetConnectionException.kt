package com.krzysztofbalana.myresume.domain.common.exceptions

class InternetConnectionException : Throwable()