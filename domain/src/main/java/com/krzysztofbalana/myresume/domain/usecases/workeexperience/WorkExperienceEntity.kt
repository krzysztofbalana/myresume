package com.krzysztofbalana.myresume.domain.usecases.workeexperience

data class WorkExperienceEntity(val title: String, val company: String, val yearFrom: String, val monthFrom: String, val yearTo: String, val monthTo: String, val descritpion: String, val position: Int)