package com.krzysztofbalana.myresume.domain.about

interface AboutApi {

    fun getAbout(): AboutEntity
}