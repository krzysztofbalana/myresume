package com.krzysztofbalana.myresume.domain.about

import com.krzysztofbalana.myresume.domain.common.Result
import com.krzysztofbalana.myresume.domain.common.UseCase
import com.krzysztofbalana.myresume.domain.common.exceptions.InternetConnectionException
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetAboutUseCase @Inject constructor(
    private val aboutApi: AboutApi
) : UseCase<Result<AboutEntity>, Unit>(Dispatchers.Main, Dispatchers.IO) {

    override suspend fun buildUseCase(param: Unit): Result<AboutEntity> {
        return try {
            val data = aboutApi.getAbout()
            if (data.description.isNotBlank()) {
                Result.Success(data)
            } else {
                Result.Failure()
            }
        } catch (e: InternetConnectionException) {
            Result.Failure(e) //normally i should handle error with specific message for ui, so it could be presented
        } catch (e: Exception) {
            Result.Failure(e)
        }
    }
}