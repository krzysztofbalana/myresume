package com.krzysztofbalana.myresume.domain.usecases.workeexperience

interface WorkExperienceApi {

    fun getWorkExperience(): List<WorkExperienceEntity>
}