package com.krzysztofbalana.myresume.domain.common

sealed class Result<T> {
    class Success<T>(val value: T) : Result<T>()
    class Failure<T>(val exception: Throwable = UnknownError()) : Result<T>()
}