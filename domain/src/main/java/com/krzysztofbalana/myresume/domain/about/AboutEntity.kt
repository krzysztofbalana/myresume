package com.krzysztofbalana.myresume.domain.about

data class AboutEntity(val description: String)