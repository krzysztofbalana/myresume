package com.krzysztofbalana.myresume.domain.common

import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

abstract class UseCase<T, P>(private val mainDispatcher: CoroutineContext, private val ioDispatcher: CoroutineContext) {

    internal abstract suspend fun buildUseCase(param: P): T

    open fun execute(param: P, callback: (T) -> Unit) {
        GlobalScope.launch(mainDispatcher) {
            try {
                val task = async(ioDispatcher) { buildUseCase(param) }

                val result = task.await()
                callback.invoke(result)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}