package com.krzysztofbalana.myresume.domain.usecases.workeexperience

import com.krzysztofbalana.myresume.domain.common.Result
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import kotlin.Exception

class GetWorkExperienceUseCaseTest {

    lateinit var systemUnderTest: GetWorkExperienceUseCase
    val workExperienceProvider: WorkExperienceApi = mock {
        on { getWorkExperience() } itReturns listOf(makeWorkExperienceEntityModel(2), makeWorkExperienceEntityModel(1), makeWorkExperienceEntityModel(4))
    }

    @Before
    fun setUp() {
        systemUnderTest = GetWorkExperienceUseCase(workExperienceProvider)
    }

    @Test
    fun `should return success when successfully fetched work experience entities`() {

        runBlocking {
            val result = systemUnderTest.buildUseCase(Unit)
            assertThat(result).isInstanceOf(Result.Success::class.java)
            assertThat((result as Result.Success).value).isNotEmpty
        }
    }

    @Test
    fun `should return data sorted ascending by position parameter`() {
        runBlocking {
            val result = systemUnderTest.buildUseCase(Unit)
            val values = (result as Result.Success).value
            assertThat(values).isSortedAccordingTo { p0, p1 -> p0.position.compareTo(p1.position) }
        }
    }

    @Test
    fun `should return result failure when something goes wrong`() {
        Mockito.`when`(workExperienceProvider.getWorkExperience()).thenAnswer { answer -> throw Exception() }

        runBlocking {
            val result = systemUnderTest.buildUseCase(Unit)
            assertThat(result).isInstanceOf(Result.Failure::class.java)
        }
    }

    private fun makeWorkExperienceEntityModel(position: Int) =
        WorkExperienceEntity("", "", "", "", "", "", "", position)
}