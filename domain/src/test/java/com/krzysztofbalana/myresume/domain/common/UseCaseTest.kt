package com.krzysztofbalana.myresume.domain.common

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.Verify
import org.amshove.kluent.on
import org.amshove.kluent.that
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.CoroutineContext

class UseCaseTest {
    lateinit var systemUnderTest: TestUseCase
    val mockedCallback: (String) -> Unit = mock()

    @Before
    fun setUp() {
        systemUnderTest = TestUseCase(
            Dispatchers.Unconfined,
            Dispatchers.Unconfined
        )
    }

    @Test
    fun `should return type of Result`() {
        runBlocking { systemUnderTest.execute(false, mockedCallback) }

        Verify on mockedCallback that mockedCallback.invoke(any())
    }

    class TestUseCase(uiContext: CoroutineContext, ioContext: CoroutineContext) :
        UseCase<String, Boolean>(uiContext, ioContext) {
        override suspend fun buildUseCase(param: Boolean): String {
            return ""
        }
    }
}